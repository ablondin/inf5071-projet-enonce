# Titre de votre jeu

## Description du projet

Décrire brièvement le contexte dans lequel votre projet de session est fait
(cours, sigle, session, enseignant, etc.).

Indiquer le moteur de jeu utilisé, ainsi que les autres logiciels que vous
prévoyez utiliser.

## Auteurs

Indiquer ici les noms et codes permanents de tous les membres de l'équipe.

## Concept

Indiquer ici le concept général de votre jeu, son objectif, l'univers dans
lequel il se déroule, etc. Mentionnez le style du jeu, ainsi que les jeux qui
vous ont inspiré. Finalement, discutez brièvement de la mécanique de jeu
principale que vous voulez développer.

## Division des tâches

Indiquez ici la division des tâches prévues. Pas besoin d'être spécifique,
simplement indiquer les points principaux. Précisez également le moyen que vous
comptez utiliser pour communiquer entre vous (Courriels, Slack, requêtes
d'intégration). Finalement, indiquez le « flux de travail » (*workflow*) que
vous préconiserez pour l'intégration du code (requêtes d'intégration, par
exemple, *issues*, etc.)

## Licence

Indiquez ici les licences utilisées pour les ressources et le code. N'oubliez
pas d'inclure un lien vers le texte de la licence, ou copiez directement le
texte de la licence dans votre dépôt.
