# Projet de session

L'objectif du projet de session est de mettre en application les notions
d'infographie vues en cours dans la conception d'un jeu vidéo 3D. Il peut
s'effectuer **seul** ou en équipe d'**au plus 5** étudiants.

Ce projet compte pour **50%** de votre résultat final. Il sera évalué en quatre
temps:

* Une **description sommaire** de votre projet (**5%**) dans laquelle vous
  décrivez votre planification, les membres de l'équipe, le style de jeu, etc.
- Une **première présentation** orale (**15%**) durant laquelle vous
  présenterez une première version (un prototype) de votre jeu. Vous devrez en
  particulier en faire une démonstration en direct pour montrer votre
  avancement.
- Une **deuxième présentation** orale (**20%**) durant laquelle vous
  présenterez une version plus aboutie de votre prototype. Celle-ci se
  déroulera devant quelques professeur.e.s et étudiant.e.s du département,
  ainsi qu'un jury amical qui évaluera la qualité de votre jeu.
- Une **remise électronique** de votre projet (**10%**) par l'intermédiaire
  d'une plateforme parmi Github, Bitbucket et GitLab.

Les objectifs principaux visés sont les suivants :

- Approfondir votre maîtrise de la **modélisation 3D**;
- Vous familiariser avec l'application de **textures 3D**;
- Vous familiariser avec la création d'**animations 3D**;
- Vous familiariser avec l'utilisation de **lumières** dans une scène;
- Vous familiariser avec le développement d'une application basée sur un
  **moteur de jeu 3D**.

## Description du projet

Vous devrez concevoir un **prototype** de jeu vidéo 3D. Le mot « prototype »
est important, car programmer une application graphique ou un jeu, même en
équipe, est un processus qui peut facilement dépasser le temps normal que vous
devriez allouer à ce cours. Par conséquent, vous pouvez présenter une
application qui inclut des bogues **mineurs** ou des éléments incomplets (c'est
donc une **preuve de concept**), mais qui donne tout de même une idée de ce que
le jeu pourrait éventuellement devenir si vous en poursuiviez le développement.

Le thème du jeu est tout à fait libre. Cependant, il n'est pas permis de
simplement **cloner** un jeu déjà existant. Vous pouvez évidemment vous
inspirer de jeux que vous connaissez, mais il doit y avoir une certaine
originalité.

D'autre part, vous devrez identifier les **styles** de jeux vidéos que vous
souhaitez implémenter. À titre indicatif, en voici quelques-uns (rien n'empêche
d'en combiner plusieurs) :

- Jeu de plateformes (*platformer*);
- Jeu de tir (*shoot'em up*);
- Jeu de course;
- Jeu de rôle (*RPG*);
- Défense de zone (*tower defense*);
- Rythmique;
- Stratégique;
- Sport;
- À tour de rôle (*turn based game*);
- Simulation (*sims*);
- Réalité virtuelle, etc.

**Suggestion**: Une bonne idée de projet consiste à construire un jeu qui est
typiquement en 2D, mais d'en faire une version 3D dans laquelle la caméra et/ou
les mouvements du personnage sont relativement limités. De cette façon, vous
pouvez profiter de la 3e dimension pour créer certains effets tout en
simplifiant le développement de votre jeu.

## Ressources (assets)

Une grande partie de l'évaluation portera sur la qualité des ressources
graphiques utilisées dans l'application graphique.

Une contrainte importante à respecter est que vous ne pouvez utiliser que des
images et des modèles que vous avez produit vous-même, que ce soit manuellement
(avec Krita, Gimp, Inkscape, Blender ou autre) ou de façon automatisée. La
seule exception à cette règle: vous avez le droit de prendre des images qui
sont déjà disponibles en leur appliquant différentes transformations ou
différents filtres de sorte que le résultat soit fortement différent de
l'original. Dans ce cas, il est important de bien citer vos sources.

Comme la conception de ressources est un processus très long, il est peu
probable que vous réussissiez à concevoir un jeu complet dans le cadre du
cours. Il est donc recommandé de limiter ce qui sera exploré dans le prototype
en utilisant différentes stratégies telles que :

- Limiter le nombre de scènes présentées lors de la preuve de concept;
- Compléter certains éléments de scène avec des ressources simplifiées
  (appelées *stubs* ou *mockup*, qui sont typiquement des sphères, des cubes,
  des cônes, etc.), suggérant que celles-ci seront éventuellement remplacées;
- Réutiliser certaines ressources en les modifiant légèrement (par exemple, un
  personnage peut être utilisé plusieurs fois en changeant simplement ses
  couleurs ou sa taille).

Il sera important de présenter à la classe votre flux de travail pour produire
ces ressources graphiques, en répondant aux questions suivantes :

- Comment avez-vous organisé vos fichiers et divisé le travail?
- Comment avez-vous procédé pour la modélisation?
- De quelle manière vous y êtes-vous pris pour appliquer des textures et des
  couleurs?
- Comment avez-vous construit vos animations?
- Quelle configuration de lumière a été employée?
- De quelle façon la caméra est-elle positionnée?
- De quelle façon avez-vous automatisé certaines parties du travail
  (utilisation de scripts, par exemple)?

## Moteur de jeu

Votre jeu doit être développé à l'aide d'un moteur de jeu 3D, c'est-à-dire un
*framework* qui facilite le développement de jeux vidéos.

Celui-ci peut être basé sur le langage et l'environnement de votre choix, en
autant que vous utilisiez une version **gratuite** que je puisse installer sur
ma machine pour tester votre jeu. Nous avons vu en classe le moteur Godot, mais
il existe plusieurs autres moteurs de jeu, par exemple

- [Unreal](https://www.unrealengine.com/) (C, C++, C#)
- [Unity](http://unity3d.com/) (C, C++, C#)
- [Panda 3D](https://www.panda3d.org/>) (Python)

## Précisions additionnelles

- Prenez le temps de bien vous accorder sur la **propriété intellectuelle** de
  ce que vous développerez. Dans l'idéal, il serait préférable que tout le code
  et toutes les ressourches (*assets*) appartiennent à l'**ensemble des
  membres** de l'équipe, mais il est possible de souhaiter une solution plus
  modulaire.
- Ajoutez une license dans le dépôt lors de la remise électronique. Je vous
  encourage à utiliser la license Creative Commons (qui couvre plusieurs **cas
  de figure**) pour les ressources graphiques et une license GPL, BSD ou MIT
  (par exemple) pour le code.
- Il est possible que je souhaite utiliser vos jeux ultérieurement dans un
  cadre pédagogique. Si c'est le cas, je vous demanderai d'abord la permission.
- Dans tous les cas, assurez-vous que le dépôt qui contient votre code est
  **privé**.
- Ne soyez pas trop ambitieux. Je vous rappelle que vous devez concevoir un
  **prototype**. Vous avez environ trois mois pour trouver une bonne idée,
  planifier le développement du jeu et le réaliser, en jonglant avec vos autres
  cours. Favorisez des idées qui sont à la fois simples et originales.
- Prévoyez développer rapidement une première version fonctionnelle (avec un
  menu, une boucle de jeu et une fin de partie) et étendez-la ensuite si vous
  avez le temps (et la motivation).
- Il n'est pas toujours facile de travailler en équipe, surtout lorsque chacun
  utilise un système d'exploitation et un environnement de développement
  différent. Accordez de l'importance, dès le début, à votre façon de
  synchroniser vos avancées via une plateforme commune (Github, Bitbucket,
  GitLab).
- En particulier, accordez de l'importance à la division des tâches et
  assurez-vous que chaque membre soit en mesure d'exécuter le jeu sur son poste
  de travail.

## Description sommaire - partie 1 (5%)

Pour la première remise, vous devez créer un dépôt Git sur une plateforme parmi
Github, Gitlab et Bitbucket et donner accès à l'utilisateur `ablondin`
(moi-même) avec droit de lecture au minimum. J'utiliserai une *issue* pour vous
donner des commentaires lors de l'évaluation.

Dans ce dépôt, pour la remise 1, on devrait y trouver au moins un fichier
`README.md` obtenu en remplissant le fichier
[`presentation.md`](./presentation.md) disponible dans le répertoire courant.

La description sommaire doit être faite au plus tard le **22 février**,
à 23h59. Dès que vous avez donné accès à l'utilisateur `ablondin`, vous pouvez
considérer le projet remis. N'hésitez pas à me demander de vérifier si j'ai
bien l'accès en m'envoyant un courriel.

## Présentations - parties 2 (15%) et 3 (20%)

Les deux présentations de projet comptent pour **35%** de votre résultat final.
La première se déroulera le **19 mars 2019** et la seconde le **23 avril
2019**.

Tel que mentionné en classe, mes collègues du département d'informatique ainsi
que les étudiants de premier cycle et de cycles supérieurs seront invités à
assister à la seconde présentation.

**Déroulement**. Chaque équipe aura droit à un temps de **5n + 2** minutes, où
**n** est le nombre d'étudiants formant l'équipe. Ainsi, une équipe de
3 personnes aura environ 17 minutes, alors qu'une équipe de 4 personnes aura
environ 22 minutes. Il est important de respecter ce temps pour ne pas trop
déborder sur l'horaire.

**Support**. Votre présentation doit être faite à l'ordinateur, en utilisant
un logiciel tel que PowerPoint, Impress, Prezi, Beamer, Reveal, etc.

**Contenu de la 1re présentation**. Vous devrez discuter minimalement des
éléments suivants:

- La division des tâches au sein de l'équipe;
- Les styles de jeu utilisés (voir plus haut);
- Des jeux déjà existants similaires à celui que vous concevez;
- Le langage de programmation et les technologies utilisées;
- Votre conception d'au moins un modèle, avec textures;
- Votre conception d'au moins une animation;
- Le jeu de lumière utilisé;
- Une démonstration en direct (prévoir au moins 5 minutes, mais idéalement
  plus), qui mettra en évidence la mécanique du jeu (*game play*);
- Ce que vous projetez faire dans le prochain mois.

**Contenu de la 2e présentation**. Ne pas trop consacrer de temps sur ce qui
a déjà été présenté dans la première partie, mais rappeler rapidement les faits
saillants, en tenant compte en particulier des éléments suivants:

- La division des tâches au sein de l'équipe;
- Les styles de jeu utilisés (voir plus haut);
- Des jeux déjà existants similaires à celui que vous concevez;
- Le langage de programmation et les technologies utilisées;
- Une présentation rapide des principaux modèles utilisés dans votre jeu;
- Une présentation rapide de votre conception d'une animation d'un système
  articulé habillé (*rigging/skinning*) **et/ou** une présentation rapide de
  votre conception d'une animation de déformation (*morph*);
- Les effets de lumière utilisés;
- L'utilisation d'au moins une *carte de normales*;
- Une démonstration en direct (prévoir au moins 5 minutes, mais idéalement
  plus), qui mettra en évidence la mécanique du jeu (*game play*);
- Les réalisations techniques et/ou artistiques dont vous êtes fiers/fières!
- Les points forts et les points faibles de votre projet;

**Conseils**. Voici quelques suggestions pour concevoir une présentation de
bonne qualité:

- Soyez dynamiques et positifs;
- Établissez un plan de votre présentation avant d'y ajouter le contenu;
- Prévoyez une minute par diapositive (en moyenne);
- Évitez les changements trop fréquents d'orateur/oratrice;
- Ne surchargez pas les diapositives avec du texte (préférer les images);
- Soignez la présentation (police uniforme, fautes d'orthographe, etc.);
- **Évitez les anglicisme et le franglais** (c'est un des problèmes que
  j'observe le plus souvent lors de présentations orales). S'il n'existe
  vraiment pas de traduction, alors assurez-vous de mettre le mot en italique;
- N'hésitez pas à inclure des captures d'écran ou même des vidéos illustrant
  votre jeu
- Mettez en évidence les mots-clé;
- Évitez les animations superflues qui distraient inutilement l'auditoire;
- Évitez les détails trop techniques (vous n'avez pas beaucoup de temps!).
- Chronométrez votre temps au moins une fois avant la présentation;

## Remise électronique - partie 4 (10%)

La remise électronique de votre projet devra être effectuée au plus tard le
**30 avril 2019**, à 23h59. Il s'agit essentiellement de vous assurer que j'ai
toujours accès à votre dépôt, qui doit contenir minimalement les éléments
suivants:

- Le code source du projet;
- Toutes les ressources utilisées (images, sons, musique, etc.) en indiquant
  **clairement** d'où elles proviennent;
- Le fichier `README` remis dans la première partie, qui peut être enrichi
  autant que vous voulez. En particulier, il doit décrire le contenu de votre
  dépôt et expliquer **clairement** comment démarrer le jeu ainsi que son
  fonctionnement;
- Le support visuel (diapositives) que vous aurez utilisé pour vos deux
  présentations orales dans un répertoire bien identifié (par exemple
  `presentations`).

Et finalement, amusez-vous!
